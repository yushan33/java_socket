//package socket;

import java.io.*;
import java.net.UnknownHostException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
//伺服器端
public class SocketServer {

	public static void main(String[] args) {
		try {
			ServerSocket ss =new ServerSocket(8888);//開啟伺服器端連線
			System.out.println("開始傾聽...");
			Socket socket =ss.accept();//接受客戶端連線
			System.out.println("已有客戶端連線...");
			
			while(true) {
				DataInputStream instream = new DataInputStream(socket.getInputStream());
				DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
				Scanner in= new Scanner(System.in);
				String msg=in.next();
				outstream.writeUTF(msg);
				String getmsg=instream.readUTF();
				System.out.println("Client:"+getmsg);
				if(msg.equals("stop")||getmsg.equals("stop")) {
					socket.close();
					ss.close();
					break;
					
				}
			}
			
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
		}
	}

}
