import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client02 {
	
	static Socket socket;
	static DataOutputStream outstream;
	static DataInputStream instream;
	
	Client02(String ip,int port) throws UnknownHostException, IOException{
		socket=new Socket(ip,port);
		outstream = new DataOutputStream(socket.getOutputStream());
		instream = new DataInputStream (socket.getInputStream());			
	}
	
	public void sendMsg(String msg) throws IOException {
		outstream.writeUTF(msg);
		if(msg.equals("exit")) {
			socket.close();
			System.exit(0);
		}
	}

	public String getMsg() throws IOException{
		String gmsg=instream.readUTF();
		return gmsg;
}
	
}
