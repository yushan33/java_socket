import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server02 {
	
	static ServerSocket ss;
	static Socket socket;
	static DataOutputStream outstream;
	static DataInputStream instream;
	
	public Server02(int port) {
		try {
			ss =new ServerSocket(port);
			socket=ss.accept();
			outstream = new DataOutputStream(socket.getOutputStream());
			instream = new DataInputStream (socket.getInputStream());			
		}
		catch(IOException e) {
			System.out.println("�ǿ饢��");
		}
	}
	
	public void sendMsg(String msg) throws IOException {
			outstream.writeUTF(msg);
			if(msg.equals("exit")) {
				socket.close();
				ss.close();
				System.exit(0);
			}
	}
	
	public String getMsg() throws IOException{
			String gmsg=instream.readUTF();
			return gmsg;
	}
}
