//package chatroomHalf;

import java.io.*;
import java.net.UnknownHostException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
//伺服器端
public class ServerOne {
	static ServerSocket ss;
	static Socket socket;
	static DataOutputStream outstream;
	static DataInputStream instream;
	static int num;
	public static void main(String[] args) throws InterruptedException {
		try {
			ss =new ServerSocket(8888);//開啟伺服器端連線
			System.out.println("開始傾聽...");
			socket =ss.accept();//接受客戶端連線
			System.out.println("已有客戶端連線...");
			System.out.println("請輸入一數開始計算，直到10");
			Scanner in =new Scanner(System.in);
			num=in.nextInt();
			
			
			while(true) {
				instream = new DataInputStream(socket.getInputStream());			
				outstream = new DataOutputStream(socket.getOutputStream());			
				
				Thread.currentThread().sleep(1000);

				outstream.writeInt(num+1);//變數
				//收到訊息
				num=instream.readInt();//變數
				
				if(num>10) {
					socket.close();
					ss.close();
					break;
					
				}
				System.out.println("Client:"+num);
				
			}
			
			
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
		}
	}
	
}


