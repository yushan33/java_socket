 //package chatroom;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Server {
	static ArrayList<String> name =  new ArrayList<>();
	static ArrayList<Mythread> myth =  new ArrayList<>();
	
	public static void main(String[] args)  {
		try {
			ServerSocket ss ;
			Socket socket;
			ss =new ServerSocket(2001);
			System.out.println("開始傾聽...");
			DataInputStream instream;
			DataOutputStream outstream;
		
			while(true) {//不斷的接受client的連線，
				socket = ss.accept();
				
				instream = new DataInputStream ( socket.getInputStream() );//輸入name
				String nm = instream.readUTF();
				System.out.println(nm+"加入聊天室。");
				name.add(nm);//記錄使用者名子
							
				Mythread mythread =new Mythread(nm,socket);
				myth.add(mythread);
				mythread.start();
				
				for(int i=0;i<name.size();i++) {
					
					if(name.get(i).equals(nm)) {
						myth.get(i).outstream.writeUTF("你加入了聊天室。");
						continue;
					}else {
						myth.get(i).outstream.writeUTF(nm+"加入聊天室。");
					}
				}
				
			}
		} catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		} catch(IOException e) {
			System.out.println("傳輸失敗");
		}


	}
	
	static class Mythread extends Thread{//處理一個client端的問題
		String n;
		Socket socket;
		DataInputStream instream;
		DataOutputStream outstream;
		
		 Mythread(String na,Socket s) throws IOException{//建構子，確保取到的socket連線是剛剛接受的連線要求
			 n=na;socket=s;
			 outstream = new DataOutputStream(socket.getOutputStream());
			 instream = new DataInputStream (socket.getInputStream());
		}
		
		public void run() {
			try {
				
				while(true) {
					
					//收到此Client傳出的訊息
					String msg=instream.readUTF();
					//System.out.print(msg);
					
					//傳送給其他Client訊息
					/*for(int i=0;i<name.size();i++) {
						
						if(name.get(i).equals(n)) {
							continue;
						}else {
							myth.get(i).outstream.writeUTF(msg);
						}
					}*/
					
					if(msg.equals(n+" :"+"exit")) {
						
						int index = name.indexOf(n);
						name.remove(index);
						myth.remove(index);
						for(int i = 0;i < name.size() ; i++ ) {
							if(!name.get(i).equals(n)) {
								myth.get(i).outstream.writeUTF(n+"已經離開聊天室");
							}
							
						}
						
						break;
						
					}else if(msg.equals(n+" :"+"who")) {
						int index = name.indexOf(n);
						myth.get(index).outstream.writeUTF("目前有"+name.size()+"人在線上。");
						for(int i=0;i<name.size();i++) {
							myth.get(index).outstream.writeUTF(name.get(i)+" ");
						}
						
					}else {
						for(int i=0;i<name.size();i++) {
							
							if(name.get(i).equals(n)) {
								continue;
							}else {
								myth.get(i).outstream.writeUTF(msg);
							}
						}
					}
	
				}
			} catch(UnknownHostException e) {
				System.out.println("主機連線失敗");
			}catch (IOException e) {
				
				//讓其他人知道有人離開
				int index = name.indexOf(n);
				name.remove(index);
				myth.remove(index);				
				
				try {
					socket.close();
				}catch(IOException e1) {
					System.out.println("傳輸失敗");
				}
				
				try {
					for(int i=0;i<name.size();i++) {
						myth.get(i).outstream.writeUTF(n+"已離線。");
					}		
				}catch(IOException e1) {
					System.out.println("傳輸失敗");
				}
					
				System.out.println("傳輸失敗t");
				}
			}
		}
	
	}
		
	


