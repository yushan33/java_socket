//package chatroom;


import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;
import java.util.Scanner;


public class Client {
	static Socket socket;
	static DataOutputStream outstream;
	static DataInputStream instream;
	
	public static void main(String[] args) {
		try {
			socket = new Socket("127.0.0.1",2001);
			Scanner in =new Scanner(System.in);
			
			System.out.print("請輸入暱稱:");
			String name=in.nextLine();
			outstream =new DataOutputStream(socket.getOutputStream());
			outstream.writeUTF(name);
			
			Getmsg2 m =new Getmsg2();
			m.start();
			
			while(true) {
				outstream =new DataOutputStream(socket.getOutputStream());
				String msg1=in.nextLine();
				String msg=name+" :"+msg1;
				outstream.writeUTF(msg);
				
				if(msg1.equals("exit")) {
					System.out.print("你離開聊天室。");
					//m.interrupt();//傳到thread的例外處理
					socket.close();
					
					System.exit(0);
					break;
				}
				
				
			}
			
		}
		catch(UnknownHostException e) {
			System.out.println("main主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("main傳輸失敗");
			
		}

	}
	static class Getmsg2 extends Thread {
		public void run() {
			try {

				while(true) {
					instream =new DataInputStream(socket.getInputStream());
					String getmsg=instream.readUTF();
					System.out.println(getmsg);
					
				}
			}
			catch(UnknownHostException e) {
				System.out.println("主機連線失敗");
			}
			catch(IOException e) {
				System.out.println("傳輸失敗thread");
			}
		}
		
	}

}