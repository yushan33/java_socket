//package chatroom02;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Server02 {

	static ArrayList<String> namelist =  new ArrayList<>();//使用者清單
	static ArrayList<Mythread> myth =  new ArrayList<>();
	//Mythread為thread，每一個mythread為server負責處理對應的每一個client的判斷與轉送
	static String cut="#@";//切割封包的符號
	public static void main(String[] args) {
		
		try {
			ServerSocket server = new ServerSocket(1001);
			System.out.println("開始傾聽...");
			Socket socket;
			DataInputStream instream;
			DataOutputStream outstream;
			
			while(true) {
				
				socket=server.accept();
				instream = new DataInputStream ( socket.getInputStream() );
				outstream= new DataOutputStream (socket.getOutputStream());
				
				//判斷有無重名
				boolean rename=false;//若名子重複，rename=true
				String packet = instream.readUTF();
				System.out.println(packet);
				String[] datas = packet.split(cut);
				
				/*請求名子封包:1,name，
				client連線時會確認名子與其他聊天室成員有無重複*/
				if(datas[0].equals("1")) {
					for(int i=0;i<namelist.size();i++) {
						if(datas[1].equals(namelist.get(i))) {
							rename=true;
							outstream.writeUTF("0"+cut+"名子不可使用!");//名子重複封包:0,"名子不可使用!"
							System.out.println("0"+cut+"名子不可使用!");
							break;
						}
					}
					if(rename==false) {//名子不重複的話
						
						namelist.add(datas[1]);
						Mythread mythread=new Mythread(datas[1],socket);
						myth.add(mythread);
						mythread.start();

						System.out.println(datas[1]+"進入了聊天室");
						int index=namelist.indexOf(datas[1]);
						//告訴其他人誰進入聊天室
						for(int i=0;i<namelist.size();i++) {
							if(i==index) {
								myth.get(i).outstream.writeUTF("1"+cut+"歡迎進入聊天室!"+cut+"");
							}else {
								myth.get(i).outstream.writeUTF("1"+cut+datas[1]+cut+"進入了聊天室!");
							}
						}
						
						String str =datas[1]+"進入了聊天室";
					    File saveFile=new File("Server.txt");
					    try{
					    	FileWriter fwriter=new FileWriter(saveFile,true);
					    	fwriter.write(str+"\r\n");
					    	fwriter.close();
				    	}catch(Exception e){
				    		System.out.println("寫檔失敗");
				    	}
					}
				}
				
				
			}
			
		} catch (IOException e) {
			System.out.println("傳輸失敗");
			
		}
		
		

	}
	
	static class Mythread extends Thread {
		String name;
		Socket socket;
		DataInputStream instream;
		DataOutputStream outstream;
		
		Mythread(String n,Socket s) throws IOException, UnknownHostException{
			 name=n;socket=s;
			 outstream = new DataOutputStream(socket.getOutputStream());
			 instream = new DataInputStream (socket.getInputStream());
			 
		}
		
		public void run(){
			try {
				while(true){
					String packet=instream.readUTF();
					//System.out.println(packet);//顯示封包
					String[] datas=packet.split(cut);
					
					
					switch(datas[0]) {
						case "2":{//傳送群聊////////////////////收到的格式為:2,寄件者,訊息
							
							for(int i=0;i<namelist.size();i++) {
								if(i!=namelist.indexOf(datas[1])) {//傳給其他人群聊訊息
									myth.get(i).outstream.writeUTF("2"+cut+datas[1]+cut+datas[2]);
								}
							}
							String str =datas[1]+":"+datas[2];
							System.out.println(str);
							File saveFile=new File("Server.txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
							
							break;
						}
						case "3":{//傳送私聊////////////////////收到的格式為:3,寄件者,收件者,訊息
							for(int i=0;i<namelist.size();i++) {
								if(i==namelist.indexOf(datas[2])) {
									myth.get(i).outstream.writeUTF("3"+cut+datas[1]+cut+datas[2]+cut+datas[3]);
								}
							}
							System.out.println(datas[1]+" 對"+datas[2]+"說 : "+datas[3]);
							
							String str =datas[1]+" 對 "+datas[2]+" 說:  "+datas[3];
							File saveFile=new File("Server.txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
							break;
						}
						case "4":{//who查詢////////////////////收到的格式為:4,name
							
							System.out.println();
							
							int index = namelist.indexOf(datas[1]);
							//System.out.println(index);
							System.out.println("目前有"+namelist.size()+"人在線上。");
							String people="";
							for(int i=0;i<namelist.size();i++) {
								people+=namelist.get(i)+",";
							}
							System.out.println(people);
							System.out.println("4"+cut+namelist.size()+cut+people);
							myth.get(index).outstream.writeUTF("4"+cut+namelist.size()+cut+people);
							
							break;
						}
						case "5":{//exit訊息
							int index= namelist.indexOf(datas[1]);
							namelist.remove(index);
							myth.remove(index);
							for(int i=0;i<namelist.size();i++) {
								myth.get(i).outstream.writeUTF("5"+cut+datas[1]);//傳送離開訊息
							}
							System.out.println(datas[1]+"同志已經下線");
							
							String str =datas[1]+"同志已經下線";
							File saveFile=new File("Server.txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
							
							break;
						}
							
					}
					
				}
				
				
				
			} catch (IOException e) {
				int index=namelist.indexOf(name);
				namelist.remove(index);
				myth.remove(index);
				
				for(int i=0;i<namelist.size();i++) {
					try {
						myth.get(i).outstream.writeUTF("5"+cut+name);
					} catch (IOException e1) {
						System.out.println("傳輸失敗");
					}
				}
				
				String str =name+"意外斷線";
				File saveFile=new File("Server.txt");
			    try{
			    	FileWriter fwriter=new FileWriter(saveFile,true);
			    	fwriter.write(str+"\r\n");
			    	fwriter.close();
		    	}catch(Exception e2){
		    		System.out.println("寫檔失敗");
		    	}
				
				
				
				try {
					socket.close();
				} catch (IOException e1) {
					System.out.println("socket關閉失敗");
				}
				
				System.out.println(name+"傳輸失敗。");
				
			}
		}
		
		
	}

}
