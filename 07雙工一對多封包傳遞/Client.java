//package chatroom02;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Scanner;

public class Client {

	static Socket socket;
	static DataOutputStream outstream;
	static DataInputStream instream;
	static String name; 
	static String cut="#@";
	static Date date = new Date();
	
	public static void main(String[] args)  {
		try {

			Boolean enter;
			do {
				socket = new Socket("127.0.0.1",1001);
				instream = new DataInputStream(socket.getInputStream());
				outstream = new DataOutputStream(socket.getOutputStream());
				
				//輸入暱稱
				System.out.print("請輸入暱稱:");
				Scanner in = new Scanner(System.in);
				String n=in.nextLine();
				String p="1"+cut+n;
				outstream.writeUTF(p);
				//System.out.println(p);
				
				//確認有無重複
				enter=true;
				String packet=instream.readUTF();
				String[] datas =packet.split(cut);
				if(datas[0].equals("0")) {
					enter=false;
					//System.out.println("enter"+enter);
				}
				else {
					name=n;
				}
			}while(enter==false);
			
			
			//開啟收訊
			GetPacket re = new GetPacket();
			re.start();
			//進入傳訊
			
			SendPacket se = new SendPacket();
			se.start();
	
		} catch (UnknownHostException e1) {
			System.out.println("123");
		} catch (IOException e1) {
			System.out.println("斷線了");
			//socket.close();
			
		}
	}

	
	//收件
	static class GetPacket extends Thread { 
		
		public void run() {
			
			while(true) {
				
				try {
					
					instream = new DataInputStream(socket.getInputStream());
					String packet=instream.readUTF();
					String[] datas=packet.split(cut);
					
					switch(datas[0]) {
						case "1":{//收到server通知 進入了聊天室
							String str=datas[1]+datas[2];
							System.out.println(str);
							

						    File saveFile=new File("User"+name+".txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
							
							break;
						}
						case "2":{//公聊
							String str=datas[1]+"說:"+datas[2];
							System.out.println(str);
							

						    File saveFile=new File("User"+name+".txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
							break;
						}
						case "3":{//私聊
							String str=datas[1]+"偷偷跟你說: \""+datas[3]+"\"";
							System.out.println(str);
							
						    File saveFile=new File("User"+name+".txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
								
							break;
						}
						case "4":{
							System.out.println("總共"+datas[1]+"個人在線上");
							System.out.println("上線人員: "+datas[2]);
							break;
						}
						case "5":{//收到成員離開通知的封包
							String str=datas[1]+" 離開了聊天室";
							System.out.println(str);
							
						    File saveFile=new File("User"+name+".txt");
						    try{
						    	FileWriter fwriter=new FileWriter(saveFile,true);
						    	fwriter.write(str+"\r\n");
						    	fwriter.close();
					    	}catch(Exception e){
					    		System.out.println("寫檔失敗");
					    	}
							
							
							break;
						}

					}
				}catch(IOException e) {
					System.out.println("收信錯誤!");
					break;
				}	
			}
		}

	}
	
	static class SendPacket extends Thread {
		public void run() {
			
			boolean leave=false;
			String type;
			Scanner in =new Scanner(System.in); 
			
			while(leave==false){
				
					
					System.out.println("請選擇聊天模式，a為群聊，b為私聊，c為查詢線上成員，d為離開 :");
					type=in.nextLine();
					switch (type) {
						case "a":{
							try {
								String msg="";
								while(!msg.equals("exit")) {
									System.out.println("輸入文字訊息:");
									msg = in.nextLine();
									if(!msg.equals("exit")) {
										String packet="2"+cut+name+cut+msg;//2,寄件者,訊息內容
										outstream.writeUTF(packet);
										String str = name+":"+msg;
									    File saveFile=new File("User"+name+".txt");
									    try{
									    	FileWriter fwriter=new FileWriter(saveFile,true);
									    	fwriter.write(str+"\r\n");
									    	fwriter.close();
								    	}catch(Exception e){
								    		System.out.println("寫檔失敗");
								    	}
									}
									
								}
							} catch (IOException e) {
								System.out.println("群聊失敗");
							}
							break;
						}
						case "b":{
							System.out.println("想傳送給誰:");
							String r=in.nextLine();
							String msg="";
							try {
								while(!msg.equals("exit")) {
									System.out.println("輸入密聊訊息:");
									msg = in.nextLine();
									if(!msg.equals("exit")) {
										String packet="3"+cut+name+cut+r+cut+msg;//3,寄件者,收件者,訊息內容
										outstream.writeUTF(packet);
										
										String str ="你偷偷對"+r+":"+msg;
									    File saveFile=new File("User"+name+".txt");
									    try{
									    	FileWriter fwriter=new FileWriter(saveFile,true);
									    	fwriter.write(str+"\r\n");
									    	fwriter.close();
								    	}catch(Exception e){
								    		System.out.println("寫檔失敗");
								    	}
									}
								}
							} catch (IOException e) {
								System.out.println("私聊失敗");
							}
							break;
						}
						
						case "c":{//who查詢
							
							try {
								outstream.writeUTF("4"+cut+name);
								Thread.sleep(1000);//
							}catch(InterruptedException e) {
								
							}
							catch (IOException e) {
								System.out.println("who 查詢失敗");
							}
							break;
						}
						
						case "d":{
							try{
								outstream.writeUTF("5"+cut);
								
								String str = "你已經下線";
							    File saveFile=new File("User"+name+".txt");
							    try{
							    	FileWriter fwriter=new FileWriter(saveFile,true);
							    	fwriter.write(str+"\r\n");
							    	fwriter.close();
						    	}catch(Exception e){
						    		System.out.println("寫檔失敗");
						    	}
								
							    socket.close();
								System.out.println("服務已經斷線!");
								leave=true;
								System.exit(0);
							}catch (IOException e) {
								System.out.println("離開失敗");
							}
							
							break;
						}
						default:{
							System.out.println("無此選項，請重新輸入");
							break;
						}
					}
		
			}
		}
	}
	

}
