//package charoomPrivate;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Server {
	static ArrayList<String> name =  new ArrayList<>();
	static ArrayList<Mythread> myth =  new ArrayList<>();
	
	public static void main(String[] args)  {
		try {
			ServerSocket ss ;
			Socket socket;
			ss =new ServerSocket(1000);
			System.out.println("開始傾聽...");
			DataInputStream instream;
			DataOutputStream outstream;
		
			while(true) {//不斷的接受client的連線，
				socket = ss.accept();
				
				instream = new DataInputStream ( socket.getInputStream() );//輸入name
				String nm = instream.readUTF();
				System.out.println(nm+"加入聊天室。");
				name.add(nm);//記錄使用者名子
							
				Mythread mythread =new Mythread(nm,socket);
				myth.add(mythread);
				mythread.start();
				
			}
			
		} catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		} catch(IOException e) {
			System.out.println("傳輸失敗");
		}


	}
	
	static class Mythread extends Thread{//處理一個client端的問題
		String n;
		Socket socket;
		DataInputStream instream;
		DataOutputStream outstream;
		
		 Mythread(String na,Socket s) throws IOException{
			 n=na;socket=s;
			 outstream = new DataOutputStream(socket.getOutputStream());
			 instream = new DataInputStream (socket.getInputStream());
		}
		
		public void run() {
			try {
				
				while(true) {
					String user=instream.readUTF();
					
					int userid = name.indexOf(user);
					
					

					myth.get(userid).outstream.writeInt(0);
					String msg=instream.readUTF();
						
						
					if(msg.equals(n+" :"+"exit")) {
						
						int index = name.indexOf(n);
						
						for(int i = 0;i < name.size() ; i++ ) {
							if(!name.get(i).equals(n)) {
								myth.get(i).outstream.writeUTF(n+"已經離開聊天室");
							}
							
						}
						name.remove(index);
						myth.remove(index);
						break;
						
					}else if(msg.equals(n+" :"+"who")) {
						int index = name.indexOf(n);
						myth.get(index).outstream.writeUTF("目前有"+name.size()+"人在線上。");
						for(int i=0;i<name.size();i++) {
							myth.get(index).outstream.writeUTF(name.get(i)+" ");
						}
						
					}else {
						myth.get(userid).outstream.writeUTF(msg);
					}
							
							
					
					
					
				}
				
				
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	
	}
		
	

}

