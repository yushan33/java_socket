//package socket;
import java.net.UnknownHostException;
import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.Scanner;
public class Server {
	static ServerSocket ss;
	static Socket socket;
	static DataOutputStream outstream;
	static DataInputStream instream;
	
	public static void main(String[] args) {
		try {
			ServerSocket ss = new ServerSocket(200);
			System.out.println("開始傾聽...");
			socket=ss.accept();//接受客戶端連線
			System.out.println("已有客戶端連線");
			Scanner in =new Scanner(System.in);
			
			Getmsg m =new Getmsg();
			m.start();
			
			while(true) {
				outstream =new DataOutputStream (socket.getOutputStream());
				//System.out.print("輸入訊息:");
				String msg = in.next();
				outstream.writeUTF(msg);
				
				if(msg.equals("exit")) {
					socket.close();
					ss.close();
					break;
				}
				

			}
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
		}

	}
	
	static class Getmsg extends Thread{
		public void run() {
			try {
				
				while(true) {
					instream =new DataInputStream(socket.getInputStream());
					String gmsg=instream.readUTF();
					System.out.println("Client:"+gmsg);
					
					if(gmsg.equals("exit")) {
						System.out.println("對方已離線");
						socket.close();
						System.exit(0);
						break;
					}
					
				}
			}
			catch(UnknownHostException e) {
				System.out.println("主機連線失敗");
				
			}
			catch(IOException e) {
				System.out.println("傳輸失敗");
				
			}
		}
	}

}


