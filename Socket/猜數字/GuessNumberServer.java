//package socket;

import java.net.Socket;
import java.util.Random;
import java.util.Scanner;
import java.net.*;
import java.io.*;

public class GuessNumberServer {

	public static void main(String[] args) {
		try {
			ServerSocket ss =new ServerSocket(1587);
			System.out.println("開始傾聽...");
			Socket socket=ss.accept();
			System.out.println("接受客戶端連線。");
			Random ran=new Random();
			int num=ran.nextInt(100);
			System.out.print("猜數字，答案為:"+num);
			while(true) {
				DataInputStream instream=new DataInputStream(socket.getInputStream());
				DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
				
				//outstream.writeUTF("猜數字1~100");
				String getmsg=instream.readUTF();
				int gnum=Integer.valueOf(getmsg);
				
				if(gnum>num) {
					outstream.writeUTF("數字太大了喔~");
				}
				if(gnum<num) {
					outstream.writeUTF("數字太小了喔~");
				}
				if(gnum==num) {
					outstream.writeUTF("Game over!");
					socket.close();
					ss.close();
					break;
				}
				
			}
			
			
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
			
		}
		
	}

}
