//package ex5;

import java.util.Random;
import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;

public class Work2 {

	public static void main(String[] args) throws IOException {
		Random ran = new Random();
		String computerNum=" ";
		boolean same=false;
		Scanner in=new Scanner(System.in);
		
		do {
			//產生一四位數的亂數
			computerNum=Integer.toString((int)(Math.random()*(9999-1001+1)+1001));

			
			//檢查亂數是否重複
			String a[]=new String[4];
			int s=0;
			for(int i=0;i<4;i++) {
				a[i]=computerNum.substring(i,i+1);
//				System.out.print(a[i]+" ");	
			}
//			System.out.print("\n");
			for(int i=0;i<4;i++) {
				for(int j=i+1;j<4;j++) 
					if(a[i].equals(a[j]))	{
						s++;
					}
			}
			
			if(s!=0) same=true;
			else same=false;
			
		}while(same==true);//若重複就重新產生新亂數
		
		System.out.println("猜數字，答案為:"+computerNum);
		
		try {
			ServerSocket ss=new ServerSocket(1001);
			System.out.println("開始傾聽...");
			Socket socket=ss.accept();
			System.out.println("接受客戶端連線");
			
			
			while(true) {
				DataInputStream instream = new DataInputStream(socket.getInputStream());
				DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
				int A=0,B=0;
				String guessNum=instream.readUTF();
				
				if(guessNum.length()==4) {
					for(int i=0;i<4;i++) {
						if(computerNum.charAt(i)==guessNum.charAt(i)) {
							A++;
						}
					}
					
					for(int i=0;i<4;i++) {
						for(int j=0;j<4;j++) {
							if(computerNum.charAt(i)!=guessNum.charAt(i)
									&&computerNum.charAt(i)==guessNum.charAt(j)) {
								B++;
							}
						}
					}
					
					if(A==4) {
						outstream.writeUTF("正確");
						
					}
					else outstream.writeUTF(A+"A"+B+"B");
					
				}else outstream.writeUTF("長度要四位數!");
				
				
			}
			
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗!");
		}
		 catch (IOException e) {
			 System.out.println("主機連線失敗!");
         }
	
		

	}


}
