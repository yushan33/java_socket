//package ex5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownServiceException;
import java.util.Scanner;

public class Work2C {

	public static void main(String[] args) {
		try {
			Socket socket =new Socket("127.0.0.1",1005);
			String gmsg;
			Scanner in= new Scanner(System.in);
			while(true) {
				DataInputStream instream =new DataInputStream(socket.getInputStream());
				DataOutputStream outstream=new DataOutputStream(socket.getOutputStream());
				String msg=in.next();
				
				//判斷數字是否重複
				boolean same=true;
				String a[]=new String[4];
				int s=0;
				for(int i=0;i<4;i++) {//將字串切開分別存進string 陣列
					a[i]=msg.substring(i,i+1);	
				}

				for(int i=0;i<4;i++) {//比較是否有數字相同
					for(int j=i+1;j<4;j++) 
						if(a[i].equals(a[j]))	{
							s++;
						}
				}
				
				if(s!=0) same=true;
				else same=false;
				
				
				if(msg.length()==4 && same==false) {
					outstream.writeUTF(msg);
					gmsg=instream.readUTF();
					System.out.println(gmsg);
					if(gmsg.equals("正確")) {
						socket.close();
						break;
					}
				}
				
				else {
					System.out.println("輸入格式錯誤，不可重複或長度不等於4");
				}
				
				
			}
		}
		catch(UnknownServiceException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
		}

	}

}
