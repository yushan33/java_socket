//package socket;

import java.io.*;
import java.net.UnknownHostException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
//半雙工伺服器端
public class SocketServer {

	public static void main(String[] args) {
		try {
			ServerSocket ss =new ServerSocket(8888);//開啟伺服器端連線
			System.out.println("開始傾聽...");
			
			Socket socket =ss.accept();//接受客戶端連線
			System.out.println("已有客戶端1連線...");
			
			Socket socket2 =ss.accept();
			System.out.println("已有客戶端2連線...");
			
			while(true) {
				DataInputStream instream = new DataInputStream(socket.getInputStream());
				DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
				
				DataInputStream instream2 = new DataInputStream(socket2.getInputStream());
				DataOutputStream outstream2 = new DataOutputStream(socket2.getOutputStream());
//				Scanner in= new Scanner(System.in);
//				String msg=in.next();
				
				
				//收到client_1的訊息傳給client_2
				String msg=instream.readUTF();
				outstream2.writeUTF(msg);
				
				//收到client_2的訊息傳給client_1
				String msg2=instream2.readUTF();
				outstream.writeUTF(msg2);
				
				if(msg.equals("stop")||msg2.equals("stop")) {
					socket.close();
					ss.close();
					break;
					
				}
			}
			
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
		}
	}

}
