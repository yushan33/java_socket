

import java.io.*;
import java.net.UnknownHostException;
import java.net.ServerSocket;
import java.net.Socket;
//伺服器端
public class SocketServer {

	public static void main(String[] args) {
		try {
			ServerSocket ss =new ServerSocket(8888);//開啟伺服器端連線
			System.out.println("開始傾聽...");
			Socket socket =ss.accept();//接受客戶端連線
			System.out.println("已有客戶端連線...");
			 
			DataInputStream instream = new DataInputStream(socket.getInputStream());
			DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
			outstream.writeUTF("Hello Client");
		}
		catch(UnknownHostException e) {
			System.out.println("主機連線失敗");
		}
		catch(IOException e) {
			System.out.println("傳輸失敗");
		}
	}

}
